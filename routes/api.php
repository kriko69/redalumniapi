<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'bolsa'],function(){
    Route::get('/trabajo','BolsaTrabajoController@getBolsaTrabajo');
    Route::get('/trabajo/{id}','BolsaTrabajoController@getBolsa');
    Route::post('/trabajo','BolsaTrabajoController@postBolsaTrabajo');
});
//usuarios
Route::post('/registrar','UsuarioController@registrar');
Route::post('/login','UsuarioController@login');
Route::get('/prueba',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@prueba'
]);
Route::get('/usuarios',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@listarUsuarios'
]);
Route::get('/perfil',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@verPerfil'
]);
Route::put('/perfil',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@actualizarPerfil'
]);
Route::delete('/usuarios/eliminar/{id}',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@eliminarUsuario'
]);

//amigos
Route::get('/usuarios/amigos',[
    'middleware'=>'JWT',
    'uses'=>'AmigoController@index'
]);
Route::post('/usuarios/amigos/buscar',[
    'middleware'=>'JWT',
    'uses'=>'AmigoController@buscarAmigo'
]);


//pruebas
Route::post('/json','UsuarioController@json');
Route::post('/relacion','UsuarioController@relacion');

//Kevin
Route::get('/rol',[
    'middleware'=>'JWT',
    'uses'=>'RolController@getRoles'
]);

Route::get('/privilegio',[
    'middleware'=>'JWT',
    'uses'=>'PrivilegioController@getPrivilegios'
]);

Route::post('/foro_pregunta',[
    'middleware'=>'JWT',
    'uses'=>'Foro_PreguntaController@postForo_Pregunta']);

Route::get('/foro_pregunta',[
    'middleware'=>'JWT',
    'uses'=>'Foro_PreguntaController@getForo_Pregunta']);

Route::get('/foro_pregunta/{id}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_PreguntaController@getForo_Pregunta_especifica']);

Route::put('/foro_pregunta/{id}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_PreguntaController@actualizarPregunta'
]);

Route::delete('/foro_pregunta/{id}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_PreguntaController@eliminarPregunta'
]);

Route::post('/foro_respuesta/{id}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_RespuestaController@postForo_Respuesta']);

Route::get('/foro_respuesta/{id}',[
    'middleware' =>'JWT',
    'uses'=>'Foro_RespuestaController@getForo_Respuesta']);
    
Route::put('/foro_respuesta/{id_pregunta}/{id_respuesta}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_RespuestaController@actualizarRespuesta'
]);

Route::delete('/foro_respuesta/{id_pregunta}/{id_respuesta}',[
    'middleware'=>'JWT',
    'uses'=>'Foro_RespuestaController@eliminarRespuesta'
]);
//kevin

Route::get('/noticias','NoticiasController@getNoticias');

Route::post('/publicacion','NoticiasController@publicacion');
Route::post('/crearmensaje/{chat_id}',[
    'middleware'=>'JWT',
    'uses'=>'MensajeController@crearmensaje'
]);
Route::post('/agregar_us_a_chat/{us_id}',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@agregar_us_a_chat'
]);
Route::get('/listarmensaje/{id}',[
    'middleware'=>'JWT',
    'uses'=>'MensajeController@listarmensaje'
]);
Route::delete('/eliminarmensaje/{id}',[
    'middleware'=>'JWT',
    'uses'=>'MensajeController@eliminarmensaje'
]);
Route::get('/listar_mensajes_chat/{chat_id}',[
    'middleware'=>'JWT',
    'uses'=>'MensajeController@listar_mensajes_chat'
]);

Route::post('/crearchat',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@crearchat'
]);
Route::get('/listarchat/{id}',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@listarchat'
]);
Route::put('/actualizarchat/{id}',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@actualizarchat'
]);
Route::delete('/eliminarchat/{id}',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@eliminarchat'
]);

Route::get('/listar_chats_usuario',[
    'middleware'=>'JWT',
    'uses'=>'ChatController@listar_chats_usuario'
]);
Route::delete('/eliminar_varios_mensajes/{id}',[
    'middleware'=>'JWT',
    'uses'=>'MensajeController@eliminar_mensajes_varios'
]);

// Inicio (Anuncios)Pao
Route::post('/registrarAnuncio','AnunciosController@registrarAnuncio');
Route::get('/listarAnuncios','AnunciosController@listarAnuncios');
Route::post('/eliminarAnuncio', 'AnunciosController@eliminarAnuncio');
// Fin (Anuncios)Pao
