<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForoRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foro_respuestas', function (Blueprint $table) {
            $table->increments('foro_respuesta_id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('foro_pregunta_id')->unsigned();
            $table->string('respuesta');
            $table->boolean('estado')->default(false);
            $table->foreign('usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('foro_pregunta_id')->references('foro_pregunta_id')->on('foro_preguntas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foro_respuestas');
    }
}
