<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amigo extends Model
{
    protected $table = 'amigos';
    protected $primaryKey='amigo_id';
    protected $fillable = [
        'usuariio_id','usuario_amigo_id','estado'
    ];


    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }
}
