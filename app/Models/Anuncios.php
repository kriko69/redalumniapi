<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anuncios extends Model
{
    protected $table='anuncios';
    protected $primaryKey='anuncio_id';
    protected $fillable = [
        'usuario_id',
        'tipo',
        'titulo',
        'descripcion',
        'ubicacion',
        'carrera',
        'fecha_inicio',
        'fecha_fin',
        'estado',
    ];

    /*public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }*/
}
