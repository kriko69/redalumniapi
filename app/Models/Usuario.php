<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{


    protected $table = 'usuarios';
    protected $primaryKey='usuario_id';
    protected $fillable = [
        'carrera', 'id', 'año_egreso','email',"ci",
        'celular','telefono','ciudad','estado','persona_id'
];

    protected $hidden = [
        'password',
    ];

    public function persona()
    {
        return $this->belongsTo(Persona::class,'persona_id');
    }
    public function roles(){
        return $this->belongsToMany(Rol::class,'rol_usuario','usuario_id','rol_id');
    }

    public function chats(){
        return $this->belongsToMany(Chat::class,'chat_usuario','usuario_id','chat_id');
    }
     
    public function foro_preguntas(){
        return $this->belongsTo(Foro_Pregunta::class);
    }
    /*public function roles()
    {
        return $this->belongstoMany(Rol::Class);
    }
    PARA LA REALIZACION DE TABLAS INTERMEDIO
    */

    public function amigos()
    {
        return $this->hasMany(Amigo::class,'usuario_id');
    }
}
