<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foro_Pregunta extends Model
{
    protected $table='foro_preguntas';
    protected $primaryKey='foro_pregunta_id';
    protected $fillable = [
        'usuario_id','pregunta','estado'];

   /* public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }
    public function foro_respuestas(){
        return $this->belongsTo(Foro_Respuesta::class);
    }*/
}
