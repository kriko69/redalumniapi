<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class noticias extends Model
{
    protected $table='noticias';
    protected $primaryKey='noticia_id';
    protected $fillable = [
        'usuario_id',
        'titulo',
        'descripcion',
        'ubicacion',
        'estado',
    ];

    /*public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }*/
}
