<?php

namespace App\Helpers;
use App\Http\Controllers\dao\UsuarioDao;
use App\Models\Usuario;
use Firebase\JWT\JWT;
use PhpParser\Node\Expr\Array_;


Class JwtAuth{

    public $key;

    public function __construct()
    {
        $this->key='software-1';
    }


    public function signUp($ci,$password,$getToken=null)
    {
        //comprobarr si el usuario exite

        $usuarioDao=new UsuarioDao();

        $user = $usuarioDao->obtenerUsuarioParaToken($ci,$password);
        $signUp=false;

        if (is_object($user))
        {
            $signUp=true;
        }

        if($signUp)
        {
            $roles=array();
            foreach ($user->roles as $rol)
            {
                array_push ($roles,$rol->pivot->rol_id);
            }
            //generar token
            $tokenVerificacion=array(
                'sub' => $user->usuario_id,
                'ci' => $user->ci,
                'roles' => $roles,
                'type'=> 'verificacion',
                'iat' => time(),
                'exp' => time() + (60*60), //1 hora
            );

            $tokenRefresh=array(
                'sub' => $user->usuario_id,
                'ci' => $user->ci,
                'roles' => $roles,
                'type'=> 'refresh',
                'iat' => time(),
                'exp' => time() + (2*60*60), //2 horas
            );

            $jwt1 = JWT::encode($tokenVerificacion,$this->key,'HS256');
            $jwt2 = JWT::encode($tokenRefresh,$this->key,'HS256');
            $decoded = JWT::decode($jwt1,$this->key,array('HS256'));

            if (is_null($getToken))
            {
                $data=array(
                    'Token-Verificacion'=>$jwt1,
                    'Token-Refresh'=>$jwt2,
                    'estado'=>'Tokens creados con exito'
                );
            }else{
                $data=$decoded;

            }

        }else{
            //devolver error
            $data=array(
                'mensaje'=>'El usuario no existe.',
                'estado'=>'error'
            );
        }
        return $data;
    }


    public function checkToken($jwt,$getIdentity=false)
    {
        $auth=false;
        $decoded=null;
        try{
            $decoded = JWT::decode($jwt,$this->key,array('HS256'));
        }catch (\UnexpectedValueException $e){
            $auth=false;
        }catch (\DomainException $e){
            $auth=false;
        }


        if (is_object($decoded) && isset($decoded))
        {
            $auth=true;
        }else{
            $auth=false;
        }

        if ($getIdentity)
        {
            return $decoded;
        }


        return $auth;
    }

    public function verificarToken($jwt)
    {
        $decoded=null;
        try{
            $decoded = JWT::decode($jwt,$this->key,array('HS256'));
        }catch (\UnexpectedValueException $e){
            $decoded=false;
        }catch (\DomainException $e){
            $decoded=false;
        }
        return $decoded;
    }

}


