<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Foro_Respuesta;
use \Validator;
use http\Env\Response;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\Foro_RespuestaBl;

class Foro_RespuestaController extends Controller
{
    function postForo_Respuesta(Request $request,$id)
    {
        $validator = [
            'respuesta' => 'required'
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorForo = Validator::make($request->all(),$validator,$customMessages);
        if ($validatorForo->fails()) {
            $data=array(
                'errores'=>$validatorForo->errors()
            );
            return response()->json($data);
        }
        $id = (int) $id;
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if($payload){
            $respuesta = $request->json("respuesta");
            $id_usuario = $payload->sub;
            $foro = new Foro_Respuesta();
            $foro->foro_pregunta_id = $id;
            $foro->usuario_id=$id_usuario;
            $foro->respuesta=$respuesta;
            $foro->estado=false;
            $bl = new Foro_RespuestaBl();
            $data = $bl->agregarRespuesta($foro);
            return response()->json($data,200);
                
        }
        else{
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }
    }

    function getForo_Respuesta(Request $request,$id)
    {
        $id = (int) $id;
        $token = $request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload = $jwt->verificarToken($token);
        if($payload)
        {
            $bl = new Foro_RespuestaBl();
            return response()->json($bl->listarRespuestas($id));
        }
        else{
            $data = array(
                'mensaje'=> 'Token incorrecto'
            );
            return response()->json($data);
        }
    }

    function actualizarRespuesta(Request $request,$id_pregunta,$id_respuesta)
    { 
        $validator = [
            'respuesta' => 'required'
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorForo = Validator::make($request->all(),$validator,$customMessages);
        if ($validatorForo->fails()) {
            $data=array(
                'errores'=>$validatorForo->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id_pregunta= (int) $id_pregunta;
        $id_respuesta = (int) $id_respuesta;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $respuesta=$request->json("respuesta");
                $usuario_id = $payload->sub;
                $bl = new Foro_RespuestaBl();
                $data = $bl->actualizarRespuesta($id_pregunta,$id_respuesta,$respuesta,$usuario_id);
                return response()->json($data);
                
            }
    }

    function eliminarRespuesta(Request $request,$id_pregunta,$id_respuesta)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id_pregunta= (int) $id_pregunta;
        $id_respuesta = (int) $id_respuesta;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $usuario_id = $payload->sub;
                $bl = new Foro_RespuestaBl();
                $data = $bl->eliminarRespuesta($id_pregunta,$id_respuesta,$usuario_id);
                return response()->json($data);
            }
    }

}
