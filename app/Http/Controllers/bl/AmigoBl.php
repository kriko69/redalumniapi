<?php

namespace App\Http\Controllers\bl;



use App\Http\Controllers\dao\AmigoDao;

class AmigoBl
{
    function verMisAmigos($usuario_id)
    {
        $dao = new AmigoDao();
        $id_amigos=array();
        $user=$dao->obtenerUsuario($usuario_id);
        if(count($user->amigos)==0)
        {
            $data=array(
                'mensaje'=>'Usted no tiene amigos.'
            );
            return response()->json($data);
        }else{
            foreach ($user->amigos as $amigo)
            {
                array_push($id_amigos,$amigo->usuario_amigo_id);
            }
            return response()->json($dao->listarMisAmigosPorId($id_amigos));
        }
    }
}