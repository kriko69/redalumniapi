<?php

namespace App\Http\Controllers\bl;

use App\Http\Controllers\dao\UsuarioDao;
use App\Models\Persona;
use App\Models\Usuario;

class UsuarioBl
{


    function registro($carrera,$ci,$egreso,$nombre,$apellidos,$email,$nacimiento,$sexo,$celular,$telefono,$ciudad,$civil,$hijos,$password)
    {
        $persona = new Persona();
        $persona->nombre=$nombre;
        $persona->apellidos=$apellidos;
        $persona->fecha_nacimiento=$nacimiento;
        $persona->sexo=$sexo;
        $persona->estado_civil=$civil;
        $persona->hijos=$hijos;
        $persona->estado=false;

        $usuario = new Usuario();
        $usuario->carrera=$carrera;
        $usuario->ci=$ci;
        $usuario->año_egreso=$egreso;
        $usuario->email=$email;
        $usuario->celular=$celular;
        $usuario->telefono=$telefono;
        $usuario->ciudad=$ciudad;
        $password=$password.'software-1'; //la sal
        $usuario->password=hash('sha256',$password);
        $usuario->estado=false;



        $usuarioDao = new UsuarioDao();
        $existe= $usuarioDao->verificarExistenciaUsuario($usuario->ci);
        if($existe)
        {
            $data=array(
                'mensaje'=>'el usuario ya existe',
                'descripcion'=>'hay un usuario con ese carnet'
            );
            return $data;
        }else
        {
            return $usuarioDao->registrar($persona,$usuario);
        }
    }

    function mostrarUsuarios($usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        $usuario=$usuarioDao->obtenerUsuario($usuario_id);
        $roles = array();
        foreach ($usuario->roles as $rol){
            array_push($roles,$rol->rol_id);
        }
        if(in_array(1,$roles))
        {
            //es admin
            $usuarios =  $usuarioDao->obtenerUsuariosParaAdmin($usuario_id);
            return response()->json($usuarios);
        }else{
            $data=array(
                'mensaje'=>'Acceso restringido'
            );
            return response()->json($data);
        }
    }

    function encontrarPersonaYUsuario($usuario_id,$nombre,$apellidos,$nacimiento,$carrera,$email,$telefono,$celular)
    {
        $usuarioDao=new UsuarioDao();
        $usuario=$usuarioDao->obtenerUsuario($usuario_id);
        $persona_id=$usuario->persona_id;
        $persona=$usuarioDao->obtenerPersona($persona_id);

        $persona->nombre = $nombre;
        $persona->apellidos = $apellidos;
        $persona->fecha_nacimiento = $nacimiento;

        $usuario->carrera=$carrera;
        $usuario->email=$email;
        $usuario->telefono=$telefono;
        $usuario->celular=$celular;

        return $usuarioDao->actualizarPerfil($persona,$usuario);

    }

    function eliminarUsuarios($usuario_id,$borrar_id)
    {
        if ($borrar_id==$usuario_id)
        {
            $data=array(
                'mensaje'=>'No puede eliminarse a usted mismo.',
            );
            return response()->json($data);
        }
        $usuarioDao=new UsuarioDao();
        $usuario=$usuarioDao->obtenerUsuario($usuario_id);
        $roles = array();
        foreach ($usuario->roles as $rol){
            array_push($roles,$rol->rol_id);
        }
        if(in_array(1,$roles))
        {
            //es admin
            $usuarios =  $usuarioDao->obtenerUsuario($borrar_id);
            if ($usuarios==null)
            {
                $data=array(
                    'mensaje'=>'el usuario con id: '.$borrar_id.' no existe.',
                );
                return response()->json($data);
            }else{
                $usuario=$usuarioDao->obtenerUsuario($borrar_id);
                $persona_id=$usuario->persona_id;
                $persona=$usuarioDao->obtenerPersona($persona_id);
                $persona->estado=true;
                $usuario->estado=true;
                return $usuarioDao->eliminarUsuario($persona,$usuario);
            }

        }else{
            $data=array(
                'mensaje'=>'Acceso restringido'
            );
            return response()->json($data);
        }
    }

    function listar($us){
        $usDao=new UsuarioDao();
        return $usDao->listar($us);

    }
}
