<?php

namespace App\Http\Controllers\bl;

use App\Http\Controllers\dao\ChatDao;
use App\Http\Controllers\dao\MensajeDao;
use App\Models\Mensaje;


class MensajeBl
{


    function crear($usuario_id,$mensaje1,$chat_id)
    {
        $mensaje = new Mensaje();
        $mensaje->usuario_id = $usuario_id;
        $mensaje->chat_id = $chat_id;
        $mensaje->mensaje = $mensaje1;

        $chatDao=new ChatDao();
        if($chatDao->verificar($chat_id)){
            $mensajeDao=new MensajeDao();
            return $mensajeDao->crear($mensaje);
        }
        else
        {$data = array(
            'mensaje' => 'Chat no existe',
            'descripcion' => 'descripcion es null'
        );
            return response()->json($data, 200);
        }
    }
    function eliminar($id)
    {
        $mensajeDao=new MensajeDao();
        if($mensajeDao->verificar($id)){
            $mensaje=$mensajeDao->obtmensaje($id);
            $mensaje->estado=1;
            return $mensajeDao->eliminar($mensaje);
        }
        else{
            $data = array(
                'mensaje' => 'Mensaje no existe',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }
    }
    function eliminar_mensajes_varios($id){
        $mensajeDao=new MensajeDao();
        return $mensajeDao->eliminar_mensajes_varios($id);
    }
    function listar_por_chat($us_id,$chat_id){
        $mensajeDao=new MensajeDao();
        $chatDao=new ChatDao();
        if($chatDao->verificar_acceso($us_id,$chat_id)){
            return $mensajeDao->listar_por_chat($chat_id);
        }
        else
        {$data = array(
            'mensaje' => 'Chat no existe',
            'descripcion' => 'descripcion es null'
        );
            return response()->json($data, 200);
        }

    }
}
