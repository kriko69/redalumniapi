<?php

namespace App\Http\Controllers\bl;



use App\Http\Controllers\dao\Foro_RespuestaDao;

class Foro_RespuestaBl
{
    function agregarRespuesta($data){
        $dao = new Foro_RespuestaDao();
        return response()->json($dao->altaRespuesta($data),200);
    }

    function listarRespuestas($id){
        $dao = new Foro_RespuestaDao();
        return response()->json($dao->listarRespuestas($id));
    }
    function actualizarRespuesta($id_pregunta,$id_respuesta,$respuesta,$usuario_id){
        $dao = new Foro_RespuestaDao();
        return response()->json($dao->actualizarRespuesta($id_pregunta,$id_respuesta,$respuesta,$usuario_id));
    }
    function eliminarRespuesta($id_pregunta,$id_respuesta,$usuario_id){
        $dao = new Foro_RespuestaDao();
        return response()->json($dao->eliminarRespuesta($id_pregunta,$id_respuesta,$usuario_id));
    }
/*
    function listaPregunta($id)
    {
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->listaPregunta($id));
    }

    function actualizarPregunta($id,$pregunta,$usuario_id){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->actualizarPregunta($id,$pregunta,$usuario_id));
    }

    function eliminarPregunta($id,$usuario_id){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->eliminarPregunta($id,$usuario_id));
    }*/
}