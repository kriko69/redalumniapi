<?php

namespace App\Http\Controllers\bl;



use App\Http\Controllers\dao\Foro_PreguntaDao;

class Foro_PreguntaBl
{
    function agregarPregunta($data){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->altaPregunta($data),200);
    }

    function listarPreguntas(){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->listarPreguntas());
    }

    function listaPregunta($id)
    {
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->listaPregunta($id));
    }

    function actualizarPregunta($id,$pregunta,$usuario_id){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->actualizarPregunta($id,$pregunta,$usuario_id));
    }

    function eliminarPregunta($id,$usuario_id){
        $dao = new Foro_PreguntaDao();
        return response()->json($dao->eliminarPregunta($id,$usuario_id));
    }
/*
    function verMisAmigos($usuario_id)
    {
        $dao = new AmigoDao();
        $id_amigos=array();
        $user=$dao->obtenerUsuario($usuario_id);
        if(count($user->amigos)==0)
        {
            $data=array(
                'mensaje'=>'Usted no tiene amigos.'
            );
            return response()->json($data);
        }else{
            foreach ($user->amigos as $amigo)
            {
                array_push($id_amigos,$amigo->usuario_amigo_id);
            }
            return response()->json($dao->listarMisAmigosPorId($id_amigos));
        }
    }*/
}