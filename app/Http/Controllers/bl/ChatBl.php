<?php

namespace App\Http\Controllers\bl;

use App\Http\Controllers\dao\ChatDao;
use App\Models\Chat;


class ChatBl
{


    function crear($nombre,$descripcion,$tipo,$usuario_id)
    {
        $chat = new Chat();
        $chat->nombre = $nombre;
        $chat->descripcion = $descripcion;
        $chat->tipo = $tipo;

        $chatdao = new ChatDao();
        return $chatdao->crear($chat, $usuario_id);
    }
    function eliminar($id)
    {
        $chatDao=new ChatDao();
        if($chatDao->verificar($id)){
            $chat=$chatDao->obtchat($id);
            $chat->estado=1;
            return $chatDao->eliminar($chat);
        }
        else{
            $data = array(
                'mensaje' => 'Mensaje no existe',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }
    }
    function agregar_us_a_chat($nombre,$descripcion,$tipo,$usuario_id,$us_id)
    {
        $chat = new Chat();
        $chat->nombre = $nombre;
        $chat->descripcion = $descripcion;
        $chat->tipo = $tipo;

        $chatdao = new ChatDao();
        return $chatdao->agregar_us_a_chat($chat, $usuario_id,$us_id);
    }
    function listar($id,$us){
        $chatDao=new ChatDao();
        return $chatDao->listar($id,$us);

    }
}
