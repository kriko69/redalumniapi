<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\MensajeBl;
use App\Http\Controllers\dao\MensajeDao;
use Illuminate\Http\Request;
use App\Models\Mensaje;
use Illuminate\Support\Facades\DB;
class MensajeController extends Controller
{
    function eliminar_mensajes_varios(Request $request,$id){
            $token=$request->header('Authorization',null);
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else {
                if (is_null($id)) {
                    $data = array(
                        'mensaje' => 'Id null'
                    );
                    return response()->json($data);
                } else {
                    $mensajeBL=new MensajeBl();
                    return $mensajeBL->eliminar_mensajes_varios($id);
                }
            }
    }

     /**
    * @OA\POST(
    *     path="/api/mensajes",
    *     summary="Crear mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function crearmensaje(Request $request,$chat_id){

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $usuario_id=$payload->sub;
            $mensaje1=$request->json("mensaje");
            if(!is_null($mensaje1)){
                if(!is_null(($usuario_id))) {
                    if(!is_null(($chat_id))) {
                        $usuariobl = new MensajeBl();
                        return $data = $usuariobl->crear($usuario_id, $mensaje1, $chat_id);
                    }
                    else{
                        if(is_null($chat_id)){
                            $data=array(
                                'mensaje'=>'chat no creado',
                                'descripcion'=>'chat_id es null'
                            );}
                    }
                }
                else{
                    if(is_null($usuario_id)){
                        $data=array(
                            'mensaje'=>'chat no creado',
                            'descripcion'=>'usuario_id es null'
                        );}
                }
            }
            else{
                if(is_null($mensaje1)){
                    $data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'mensaje es null'
                    );}
            }

            return response()->json($data,200);
        }
    }
    /**
    * @OA\GET(
    *     path="/api/mensajes/{$id}",
    *     summary="Listar un mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function listarmensaje(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $mensajeDao=new MensajeDao();
            return $mensajeDao->listar($id,$payload->sub);
       }
    }
    /**
    * @OA\DELETE(
    *     path="/api/mensajes/{$id}",
    *     summary="Borrar un mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function eliminarmensaje(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            if(is_null($id))
            {
                $data=array(
                    'mensaje'=>'Id null'
                );
                return response()->json($data);
            }else {
                $mensajeBl=new MensajeBl();
                return $mensajeBl->eliminar($id);
            }
        }
    }
    public function listar_mensajes_chat(Request $request,$chat_id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $mensajeBl=new MensajeBl();
            return $mensajeBl->listar_por_chat($payload->sub,$chat_id);
        }
    }
}
