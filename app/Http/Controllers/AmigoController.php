<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\AmigoBl;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AmigoController extends Controller
{
    public function index(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl= new AmigoBl();
            return $bl->verMisAmigos($payload->sub);

        }

    }

    public function buscarAmigo(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $filtro=$request->json("filtro",null);
        $valor=$request->json("valor");

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {

            if (!isset($filtro))
            {
                $data=array(
                    'mensaje'=>'La variable filtro no existe'
                );
                return response()->json($data);
            }
            if (!isset($valor))
            {
                $data=array(
                    'mensaje'=>'La variable valor no existe'
                );
                return response()->json($data);
            }

            $id = $payload->sub;
            if (is_null($valor) && !is_null($filtro))
            {
                $data=array(
                    'mensaje'=>'Necesita poner un valor.'
                );
                return response()->json($data);
            }elseif (is_null($filtro)){
                $user=Usuario::where('usuario_id','<>',$id)
                    ->where('estado',false)
                    ->get();
                return response()->json($user);
            }else{
                $filtro=strtolower($filtro);
                $valor=strtolower($valor);
                switch ($filtro) {
                    case 'nombre':
                        $user=DB::table('usuarios')
                            ->join('persona','persona.persona_id','=','usuarios.persona_id')
                            ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                                'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera')
                            ->where('usuarios.usuario_id','<>',$id)
                            ->where('usuarios.estado','=',false)
                            ->where('persona.nombre', 'like', '%' .$valor.'%')
                            ->OrWhere('persona.apellidos', 'like', '%' .$valor.'%')
                            ->get();

                        if(count($user)==0)
                        {
                            $data=array(
                                'mensaje'=>'No se encontro amigos.'
                            );
                            return response()->json($data);
                        }else{
                            return response()->json($user);
                        }
                        break;
                    case 'carrera':
                        $user=DB::table('usuarios')
                            ->join('persona','persona.persona_id','=','usuarios.persona_id')
                            ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                                'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera')
                            ->where('usuarios.usuario_id','<>',$id)
                            ->where('usuarios.estado','=',false)
                            ->where('usuarios.carrera', 'like', '%' .$valor.'%')
                            ->get();

                        if(count($user)==0)
                        {
                            $data=array(
                                'mensaje'=>'No se encontro amigos.'
                            );
                            return response()->json($data);
                        }else{
                            return response()->json($user);
                        }
                        break;
                    case 'ci':
                        $valor=(int) $valor;
                        if($valor==0)
                        {
                            $data=array(
                                'mensaje'=>'El valor debe ser un entero positivo.'
                            );
                            return response()->json($data);
                        }else{
                            $user=DB::table('usuarios')
                                ->join('persona','persona.persona_id','=','usuarios.persona_id')
                                ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                                    'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera')
                                ->where('usuarios.usuario_id','<>',$id)
                                ->where('usuarios.estado','=',false)
                                ->where('usuarios.ci', $valor)
                                ->get();
                            if(count($user)==0)
                            {
                                $data=array(
                                    'mensaje'=>'No se encontro amigos.'
                                );
                                return response()->json($data);
                            }else{
                                return response()->json($user);
                            }
                        }

                        break;
                    default:
                        $data=array(
                            'mensaje'=>'Filtro incorrecto.'
                        );
                        return response()->json($data);
                }
            }
        }
    }
}
