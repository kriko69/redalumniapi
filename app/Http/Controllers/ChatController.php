<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\ChatBl;
use App\Http\Controllers\dao\ChatDao;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Chat;
use Illuminate\Support\Facades\DB;
/**
* @OA\Info(title="API RedAlumni", version="1.0")
*
* @OA\Server(url="http://localhost:8000")
*/
class ChatController extends Controller
{
     /**
    * @OA\POST(
    *     path="/api/chats",
    *     summary="Crear chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
        public function crearchat(Request $request){

            $token=$request->header('Authorization',null);
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
        $nombre=$request->json("nombre");
        $descripcion=$request->json("descripcion");
        $tipo=$request->json("tipo");
        if(!is_null($nombre) && !is_null($descripcion) && !is_null($tipo)){
            $usuario_id=$payload->sub;
            $chatBl=new ChatBl();
            return $chatBl->crear($nombre,$descripcion,$tipo,$usuario_id);
    }
    else{
        if(is_null($nombre)){
            return response()->json($data=array(
            'mensaje'=>'chat no creado',
            'descripcion'=>'nombre es null'
        ),200);}
        else if(is_null($descripcion)){
            return response()->json($data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'descripcion es null'
            ),200);}
        else if(is_null($tipo)){
            return response()->json($data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'tipo es null'
            ),200);}
        if(is_null($descripcion) && is_null($nombre) && is_null($tipo)){
            return response()->json($data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'descripcion es null'
            ),200);
        }

    }
            }
    }
    /**
    * @OA\GET(
    *     path="/api/chats",
    *     summary="Listar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function listarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chatBL=new ChatBl();
            return $chatBL->listar($id,$payload->sub);
        }
    }
    /**
    * @OA\PUT(
    *     path="/api/chats",
    *     summary="Actualizar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function actualizarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat = Chat::where('chat_id','=',$id)->first();


            if(is_object($chat)){
            $nombre=$request->json("nombre");
            $descripcion=$request->json("descripcion");


            if ($nombre!=null) {
                $chat->nombre = $nombre;
            }
            if ($descripcion!=null) {
                $chat->descripcion = $descripcion;
            }
            if($nombre==null && $descripcion==null)
            {
                return response()->json(['error' => 'No hay nada para cambiar.', 'code' => 200]);
            }
            if (!$chat->isDirty()) {
                return response()->json(['error' => 'No hay nada cambiado.', 'code' => 200]);
            }
                        $chat->save();
                return response()->json(['exito' => 'Chat cambiado.','chat'=>$chat,'code'=>200]);
        }
            else{
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    /**
    * @OA\DELETE(
    *     path="/api/chats",
    *     summary="Borrar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function eliminarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {if(is_null($id))
        {
            $data=array(
                'mensaje'=>'Id null'
            );
            return response()->json($data);
        }else {
            $chatBl=new ChatBl();
            return $chatBl->eliminar($id);
        }
        }
    }
    public function agregar_us_a_chat(Request $request,$us_id){

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $nombre=$request->json("nombre");
            $descripcion=$request->json("descripcion");
            $tipo=$request->json("tipo");
            if(!is_null($nombre) && !is_null($descripcion) && !is_null($tipo)){
                $usuario_id=$payload->sub;
                $chatBl=new ChatBl();
                return $chatBl->agregar_us_a_chat($nombre,$descripcion,$tipo,$usuario_id,$us_id);
            }
            else{
                if(is_null($nombre)){
                    return response()->json($data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'nombre es null'
                    ),200);}
                else if(is_null($descripcion)){
                    return response()->json($data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'descripcion es null'
                    ),200);}
                else if(is_null($tipo)){
                    return response()->json($data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'tipo es null'
                    ),200);}
                if(is_null($descripcion) && is_null($nombre) && is_null($tipo)){
                    return response()->json($data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'descripcion es null'
                    ),200);
                }

            }
        }
    }
    public function listar_chats_usuario(Request $request){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $usuario_id = $payload->sub;
            $chatDao=new ChatDao();
            return $chatDao->listar_por_us($usuario_id);

        }
    }
    /*public function agregar_participante(Request $request,$id,$id2){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat = Chat::where('chat_id','=',$id)->first();
            if(is_object($chat)){
                $nombre=$request->json("nombre");
                $descripcion=$request->json("descripcion");


                if ($nombre!=null) {
                    $chat->nombre = $nombre;
                }
                if ($descripcion!=null) {
                    $chat->descripcion = $descripcion;
                }
                if($nombre==null && $descripcion==null)
                {
                    return response()->json(['error' => 'No hay nada para cambiar.', 'code' => 200]);
                }
                if (!$chat->isDirty()) {
                    return response()->json(['error' => 'No hay nada cambiado.', 'code' => 200]);
                }
                $chat->save();
                return response()->json(['exito' => 'Chat cambiado.','chat'=>$chat,'code'=>200]);
            }
            else{
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }*/
}
