<?php

namespace App\Http\Controllers;

use App\Http\Controllers\bl\UsuarioBl;
use App\Http\Controllers\dao\UsuarioDao;
use App\Models\Persona;
use App\Models\Rol;
use App\Models\Usuario;
use \Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{

    public function registrar(Request $request)
    {
        $rules = [
            'carrera' => 'required',
            'año_egreso' => 'required',
            'ci' => 'required',
            'email' => 'required',
            'celular' => 'required',
            'telefono' => 'required',
            'ciudad' => 'required',
            'nombre' => 'required',
            'apellidos' => 'required',
            'fecha_nacimiento' => 'required',
            'estado_civil' => 'required',
            'hijos' => 'required',
            'password'=> 'required'
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $carrera=$request->json("carrera");
        $ci=$request->json("ci");
        $egreso=$request->json("año_egreso");
        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $email=$request->json("email");
        $nacimiento=$request->json("fecha_nacimiento");
        $sexo=$request->json("sexo");
        $celular=$request->json("celular");
        $telefono=$request->json("telefono");
        $ciudad=$request->json("ciudad");
        $civil=$request->json("estado_civil");
        $hijos=$request->json("hijos");
        $password=$request->json("password");

        //return $carrera.'-'.$ci.'-'.$egreso.'-'.$nombre.'-'.$apellidos.'-'.$email.'-'.$nacimiento.'-'.$sexo.'-'.$celular.'-'.$telefono.'-'.$ciudad.'-'.$civil.'-'.$password.'-'.$hijos;

        if(!is_null($carrera) && !is_null($ci) && !is_null($egreso) && !is_null($nombre) && !is_null($apellidos) && !is_null($email) && !is_null($nacimiento)
        && !is_null($sexo) && !is_null($celular) && !is_null($telefono) && !is_null($ciudad) && !is_null($civil) && !is_null($hijos)
        && !is_null($password)){

            $bl = new UsuarioBl();
            $data=$bl->registro($carrera,$ci,$egreso,$nombre,$apellidos,$email,$nacimiento,$sexo,$celular,$telefono,$ciudad,$civil,$hijos,$password);

        }else{

            $data=array(
                'mensaje'=>'usuario no creado',
                'descripcion'=>'algun parametro en null'
            );

        }

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    public function login(Request $request)
    {
        $ci=$request->json("ci");
        $password=$request->json("password");
        /*$rules = [
            'ci' => 'required',
            'password' => 'required',

        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $jwtAuth = new JwtAuth();


        if (!is_null($ci) && !is_null($password) && isset($ci) && isset($password))
        {
            $password=$password.'software-1';
            $password=hash('sha256',$password);
            $signUp=$jwtAuth->signUp($ci,$password);
            return response()->json($signUp,200);
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function listarUsuarios(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->mostrarUsuarios($payload->sub);
        }


    }

    public function verPerfil(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $dao=new UsuarioDao();
            $users=$dao->verPerfil($payload->sub);
            return response()->json($users);
        }

    }

    public function actualizarPerfil(Request $request)
    {
        $rules = [
            'nombre' => 'required',
            'apellidos' => 'required',
            'carrera' => 'required',
            'fecha_nacimiento' => 'required',
            'email' => 'required',
            'telefono' => 'required',
            'celular' => 'required',
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $carrera=$request->json("carrera");
        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $email=$request->json("email");
        $nacimiento=$request->json("fecha_nacimiento");
        $celular=$request->json("celular");
        $telefono=$request->json("telefono");
        $token=$request->header('Authorization',null);
        if(!is_null($carrera) && !is_null($nombre) && !is_null($apellidos) &&
            !is_null($nacimiento) && !is_null($celular) && !is_null($telefono)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->encontrarPersonaYUsuario($payload->sub,$nombre,$apellidos,$nacimiento,$carrera,$email,$telefono,$celular);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }


    }

    public function eliminarUsuario(Request $request, $id)
    {
        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => ['required','integer','min:1']
            ],
            [
                'id.required' => 'El atributo :attribute  es requerido.',
                'id.integer'=> 'el atributo :attribute debe ser un numero',
                'id.min'=> 'el atributo :attribute debe ser positivo'
         ]);
        if ($validator->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validator->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new UsuarioBl();
            return $bl->eliminarUsuarios($payload->sub,$id);

        }

    }

    public function prueba(Request $request)
    {
        $token=$request->header('Authorization',null);

        $user=Usuario::where('usuario_id','<>',3)->get();
        return response()->json($user);
    }


    public function relacion(Request $request)
    {
        /*$user = Usuario::where(
            array(
                'usuario_id'=>18
            )
        )->first();
        /*foreach ($user->roles as $rol)
        {
            echo $rol->pivot->rol_id;
        }*/

        //return response()->json($user->usuario_id);
        //return response()->json($user->roles);

        /*$user=DB::table('amigos')
            ->join('usuarios','amigos.usuario_amigo_id','=','usuarios.usuario_id')
            ->join('persona','persona.persona_id','=','usuarios.persona_id')
            ->select('persona.nombre','usuarios.ci','amigos.usuario_amigo_id')
            ->get();
        return response()->json($user);*/
        $id_amigos=array();
        $user=Usuario::find(3);
        if(count($user->amigos)>0)
        {
            foreach ($user->amigos as $amigo)
            {
                array_push($id_amigos,$amigo->usuario_amigo_id);
            }
            $user=DB::table('usuarios')
                ->join('persona','persona.persona_id','=','usuarios.persona_id')
                ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                    'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera')
                ->whereIn('usuarios.usuario_id',$id_amigos)
                ->get();
            return response()->json($user);
        }else{
            return 'no tiene amigos';
        }



    }
    public function json(Request $request)
    {
       /*
        * {
            "name": "Phill Sparks",
            "location": "England",
            "skills": [
                "PHP",
                "MySQL"
            ],
            "job": [
                {
                    "org": "Laravel",
                    "role": "Quality Team",
                    "since": 2012
                }
            ]
           }
       */

       /*$data = $request->json('skills');
        return $data[0]; //PHP */

        $data = $request->json('job');

        foreach ($data[0] as $key => $value) {
            //echo "$key => $value", PHP_EOL;
            if($key=='since')
            {
                return gettype($value);
            }
        }

    }

}
