<?php

namespace App\Http\Controllers;

// use App\Anuncios;
// use App\Models\Persona;
use App\Helpers\JwtAuth;
use App\Models\Anuncios;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AnunciosController extends Controller
{
    public function registrarAnuncio(Request $request)
    {
        // Datos
        $token=$request->json('Authorization',null);
        $usuario_id=$request->json("usuario_id");
        $tipo=$request->json("tipo");
        $titulo=$request->json("titulo");
        $descripcion=$request->json("descripcion");
        $ubicacion=$request->json("ubicacion");
        $carrera=$request->json("carrera");
        $fecha_inicio=$request->json("fecha_inicio");   // AAAA/MM/DD
        $fecha_fin=$request->json("fecha_fin");
        $estado=$request->json("estado");

        // Validator de Laravel
        $validator = Validator::make($request->all(), [
            'Authorization' => 'required',
            'usuario_id' => 'required',
            'tipo' => 'required',
            'titulo' => 'required',
            'descripcion' => 'required|min:5|max:191',
            'ubicacion' => 'required',
            'carrera' => 'required',
            'fecha_inicio' => 'required|max:10|date',
            'fecha_fin' => 'required|max:10|date',
            'estado' => 'required|max:1'
        ]);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json($messages);
        } else if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto o expirado.'
            );
            return response()->json($data);
        }else {

            $anuncio = new Anuncios();
            $anuncio->usuario_id = $usuario_id;
            $anuncio->tipo = $tipo;
            $anuncio->titulo = $titulo;
            $anuncio->descripcion = $descripcion;
            $anuncio->ubicacion = $ubicacion;
            $anuncio->carrera = $carrera;
            $anuncio->fecha_inicio = $fecha_inicio;
            $anuncio->fecha_fin = $fecha_fin;
            $anuncio->estado = $estado;

            $isset_usuario=Usuario::where('usuario_id','=',$usuario_id)->first();

            if (is_object($isset_usuario))
            {

                DB::beginTransaction();
                try {
                    $anuncio->save();
                    $data=array(
                        'mensaje'=>'El anuncio fue creado exitosamente.',
                        'descripcion'=>'exito.',
                        'id_usuario'=>$usuario_id
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                } catch (Throwable $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }else{
                $data=array(
                    'mensaje'=>'el usuario no es valido',
                    'descripcion'=>'El id del usuario no es valido'
                );
            }
        }

        return response()->json($data,200);
    }

    public function listarAnuncios(Request $request)
    {
        $token=$request->json('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        $validator = Validator::make($request->all(), [
            'Authorization' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json($messages);
        }else if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto o expirado.'
            );
            return response()->json($data);
        }else{
            // En caso de que el token sea valido, se lista todos los anuncios
            $anuncios = Anuncios::all()->where('estado','=',1);
            return response()->json($anuncios, 200);

        }

    }

     public function eliminarAnuncio(Request $request){
         $token=$request->json('Authorization',null);
         $id=(int)$request->json("anuncio_id");
         $jwt = new JwtAuth();
         $payload=$jwt->verificarToken($token);

         $validator = Validator::make($request->all(), [
             'Authorization' => 'required',
             'anuncio_id' => 'required',
         ]);
         if ($validator->fails()) {
             $messages = $validator->messages();
             return response()->json($messages);
         }else if(!$payload)
         {
             $data=array(
                 'mensaje'=>'Token incorrecto o expirado.'
             );
             return response()->json($data);
         }else {
             $anuncio =Anuncios::find($id);
             DB::beginTransaction();
             try {
                 $anuncio->estado=false;
                 $anuncio->save();
                 $data=array(
                     'mensaje'=>'El anuncio fue eliminado exitosamente.',
                     'descripcion'=>'exito.'
                 );
                 DB::commit();
             } catch (Exception $e) {
                 $data=array(
                     'mensaje'=>'Error al realizar la transaccion',
                     'descripcion'=>'fallo'
                 );
                 DB::rollback();
             } catch (Throwable $e) {
                 $data=array(
                     'mensaje'=>'Error al realizar la transaccion',
                     'descripcion'=>'fallo'
                 );
                 DB::rollback();
             }
         }
           return response()->json($data,200);
    }

}
