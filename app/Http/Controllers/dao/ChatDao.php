<?php

namespace App\Http\Controllers\dao;

use App\Models\Chat;
use Illuminate\Support\Facades\DB;

class ChatDao
{
    function crear($chat,$usuario_id)
    {
        DB::beginTransaction();
        try {
            $chat->save();
            $chat->usuarios()->attach($usuario_id);
            $data = array(
                'mensaje' => 'chat creado con exito',
                'descripcion' => 'exito',
                'chat_id' => $chat->chat_id
            );

            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }
        return response()->json($data, 200);
    }
    function listar($id,$usuario_id)
    {
        $chat=DB::table('chat_usuario')
            ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
            ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
            ->select('chats.chat_id','chats.nombre','chats.descripcion','chats.tipo')
            ->where('chats.chat_id','=',$id)
            ->where('usuarios.usuario_id','=',$usuario_id)
            ->where('chats.estado','=',0)
            ->get();
        if (count($chat)!=0) {
            return response()->json($chat, 200);
        } else {
            $data = array(
                'mensaje' => 'chat no existe',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }

    }
    function verificar($id)
    {
        $chat = Chat::where('chat_id','=',$id)->first();
        if(is_object($chat)){
            return true;
        }
        else{
            return false;
        }
    }
    function verificar_acceso($us_id,$chat_id)
    {
        $chat=DB::table('chat_usuario')
            ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
            ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
            ->select('chats.chat_id','chats.nombre','chats.descripcion','chats.tipo')
            ->where('chats.chat_id','=',$chat_id)
            ->where('usuarios.usuario_id','=',$us_id)
            ->where('chats.estado','=',0)
            ->get();
        if(is_object($chat)){
            return true;
        }
        else{
            return false;
        }
    }
    function obtchat($id)
    {
        $chat = Chat::where('chat_id','=',$id)->first();
        return $chat;
    }
    function eliminar($chat)
    {
        DB::beginTransaction();
        try {
            $chat->save();
            DB::commit();
            $data = array(
                'mensaje' => 'Chat elimnado',
                'descripcion' => 'exito'
            );
        } catch (Exception $e) {
            $data = array(
                'mensaje' => 'Error al realizar la transaccion',
                'descripcion' => 'fallo'
            );
            DB::rollback();
        }
        return response()->json($data,200);
    }
    function listar_por_us($usuario_id)
    {
        $chat=DB::table('chat_usuario')
            ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
            ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
            ->join('mensajes','mensajes.chat_id','=','chats.chat_id')
            ->select(DB::raw('count(mensajes.mensaje_id) as cantidad_mensajes,chats.nombre,chats.chat_id,chats.descripcion,chats.tipo'))
            ->where('usuarios.usuario_id','=',$usuario_id)
            ->where('chats.estado','=',0)
            ->where('mensajes.estado','=',0)
            ->groupBy('chats.nombre','chats.chat_id','chats.descripcion','chats.tipo')
            ->get();
        if (count($chat)!=0) {
            return response()->json($chat, 200);
        } else {
            $data = array(
                'mensaje' => 'chats no existen',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }
    }
    function agregar_us_a_chat($chat,$usuario_id,$us_id)
    {
        DB::beginTransaction();
        try {
            $chat->save();
            $chat->usuarios()->attach($usuario_id);
            $chat->usuarios()->attach($us_id);
            $data = array(
                'mensaje' => 'chat creado con exito',
                'descripcion' => 'exito',
                'chat_id' => $chat->chat_id
            );

            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }
        return response()->json($data, 200);
    }
}
