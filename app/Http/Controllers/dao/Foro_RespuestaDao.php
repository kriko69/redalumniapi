<?php

namespace App\Http\Controllers\dao;



use Illuminate\Support\Facades\DB;
use App\Models\Foro_Respuesta;
use App\Models\Foro_Pregunta;

class Foro_RespuestaDao
{
    function altaRespuesta($foro)
    {
        DB::beginTransaction();
                try {
                    $foro->save();
                    $data=array(
                        'mensaje'=>'Pregunta respondida con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la respuesta',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
                return response()->json($data);
    }

    function listarRespuestas($id)
    {
        $foro = Foro_Respuesta::all()->where('foro_pregunta_id','=',$id)->where('estado','=',false);
        if(json_decode($foro, true)){
            return response()->json($foro);
        }
        else {
            $data= array(
                'mensaje'=>'No existe la respuesta que busca'
            );
            return response()->json($data);
        }
    }


    function actualizarRespuesta($id_pregunta,$id_respuesta,$respuesta,$usuario_id)
    {
        $foroP = Foro_Pregunta::all()->where('foro_pregunta_id','=',$id_pregunta)->where('estado','=',false);
        if(json_decode($foroP, true)){
            $foro=Foro_Respuesta::find($id_respuesta);
            if($foro->usuario_id == $usuario_id){
                $foro->usuario_id=$usuario_id;
                $foro->respuesta=$respuesta;
                $foro->estado=false;
                DB::beginTransaction();
                try {
                    $foro->save();
                    $data=array(
                        'mensaje'=>'Respuesta actualizada con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }
            else{
                $data=array(
                    'mensaje'=>'El usuario no tiene acceso a modificar la respuesta'
                );

            }
            return response()->json($data,200);
        }
        else {
            $data= array(
                'mensaje'=>'No existe la pregunta que busca'
            );
            return response()->json($data);
        }
        
    }

    function eliminarRespuesta($id_pregunta,$id_respuesta,$usuario_id)
    {
        $foroP = Foro_Pregunta::all()->where('foro_pregunta_id','=',$id_pregunta)->where('estado','=',false);
        if(json_decode($foroP, true)){
            $foro=Foro_Respuesta::find($id_respuesta);
            if($foro->usuario_id == $usuario_id){
                $foro->usuario_id=$usuario_id;
                $foro->estado=true;
                DB::beginTransaction();
                try {
                    $foro->save();
                    $data=array(
                        'mensaje'=>'Respuesta eliminada con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la eliminacion de la respuesta',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }
            else{
                $data=array(
                    'mensaje'=>'El usuario no tiene acceso a eliminar la respuesta'
                );

            }
            return response()->json($data,200);
        }
        else {
            $data= array(
                'mensaje'=>'No existe la pregunta que busca'
            );
            return response()->json($data);
        }
    }
}
