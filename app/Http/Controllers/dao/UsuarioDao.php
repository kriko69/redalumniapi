<?php

namespace App\Http\Controllers\dao;

use App\Models\Persona;
use App\Models\Usuario;
use Illuminate\Support\Facades\DB;

class UsuarioDao
{


    function registrar($persona,$usuario)
    {
        DB::beginTransaction();
        try {
            $persona->save();
            $usuario->persona_id=$persona->persona_id;
            $usuario->save();
            $usuario->roles()->attach(2);
            $data=array(
                'mensaje'=>'persona y usuario creado con exito',
                'descripcion'=>'exito',
                'id_persona'=>$persona->persona_id,
                'id_usuario'=>$usuario->usuario_id
            );
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }
        return $data;
    }


    function verificarExistenciaUsuario($ci)
    {
        $isset_usuario=Usuario::where('ci','=',$ci)->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function obtenerUsuarioParaToken($ci,$password)
    {
        $user = Usuario::where(
            array(
                'ci' => $ci,
                'password' => $password
            )
        )->first();
        return $user;
    }

    function obtenerUsuario($usuario_id)
    {
        return Usuario::find($usuario_id);
    }
    function obtenerPersona($persona_id)
    {
        return Persona::find($persona_id);
    }
    function obtenerUsuariosParaAdmin($usuario_id)
    {
        return Usuario::all()->where('usuario_id','<>',$usuario_id); //diferentes de el
    }
    function verPerfil($usuario_id)
    {
        return DB::table('persona')
            ->join('usuarios','persona.persona_id','=','usuarios.persona_id')
            ->select('persona.nombre','persona.apellidos','usuarios.ci','usuarios.carrera',
                'persona.fecha_nacimiento','usuarios.email','usuarios.telefono','usuarios.celular')
            ->where('usuarios.usuario_id','=',$usuario_id)
            ->get();
    }

    function actualizarPerfil($persona,$usuario)
    {
        DB::beginTransaction();
        try {

            $persona->save();
            $usuario->save();
            $data=array(
                'mensaje'=>'El usuario se actualizo con exito',
                'descripcion'=>'exito'
            );
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }

        return response()->json($data);
    }

    function eliminarUsuario($persona,$usuario)
    {
        DB::beginTransaction();
        try {

            $persona->save();
            $usuario->save();
            $data=array(
                'mensaje'=>'El usuario se elimino con exito',
                'descripcion'=>'exito'
            );
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }
        return response()->json($data);
    }


}
