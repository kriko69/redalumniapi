<?php

namespace App\Http\Controllers\dao;

use App\Models\Mensaje;
use Illuminate\Support\Facades\DB;

class MensajeDao
{
    function crear($mensaje)
    {
        DB::beginTransaction();
        try {
            $mensaje->save();
            $data = array(
                'mensaje' => 'mensaje creado con exito',
                'descripcion' => 'exito',
                'mensaje_id' => $mensaje->mensaje_id
            );
            DB::commit();
        } catch (Exception $e) {
            $data = array(
                'mensaje' => 'Error al realizar la transaccion',
                'descripcion' => 'fallo'
            );
            DB::rollback();
        }
        return response()->json($data, 200);
    }
    function listar($id,$usuario_id)
    {
        $mensaje=DB::table('chat_usuario')
            ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
            ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
            ->join('mensajes','mensajes.chat_id','=','chats.chat_id')
            ->select('mensajes.mensaje_id','mensajes.chat_id','mensajes.usuario_id','mensajes.mensaje')
            ->where('mensajes.mensaje_id','=',$id)
            ->where('usuarios.usuario_id','=',$usuario_id)
            ->where('mensajes.estado','=',0)
            ->get();
        if (count($mensaje)!=0) {
            return response()->json($mensaje, 200);
        } else {
            $data = array(
                'mensaje' => 'mensaje no existe',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }

    }
    function verificar($id)
    {
        $mensaje = Mensaje::where('mensaje_id','=',$id)->first();
        if(is_object($mensaje)){
            return true;
        }
        else{
            return false;
        }
    }
    function obtmensaje($id)
    {
        $mensaje = Mensaje::where('mensaje_id','=',$id)->first();
            return $mensaje;
    }
    function eliminar_mensajes_varios($mensaje)
    {
        DB::beginTransaction();
        try {
            DB::table('mensajes')
                ->where('chat_id', $mensaje)
                ->update(array('estado' => 1));
            DB::commit();
            $data = array(
                'mensaje' => 'Mensaje elimnado',
                'descripcion' => 'exito'
            );
            } catch (Exception $e) {
                $data = array(
                    'mensaje' => 'Error al realizar la transaccion',
                    'descripcion' => 'fallo'
                );
            DB::rollback();
        }
    return response()->json($data,200);
}
    function eliminar($mensaje)
    {
        DB::beginTransaction();
        try {
            $mensaje->save();
            DB::commit();
            $data = array(
                'mensaje' => 'Mensaje elimnado',
                'descripcion' => 'exito'
            );
        } catch (Exception $e) {
            $data = array(
                'mensaje' => 'Error al realizar la transaccion',
                'descripcion' => 'fallo'
            );
            DB::rollback();
        }
        return response()->json($data,200);
    }
    function listar_por_chat($chat_id)
    {
        $mensaje=DB::table('mensajes')
            ->join('chats','chats.chat_id','=','mensajes.chat_id')
            ->join('usuarios','usuarios.usuario_id','=','mensajes.usuario_id')
            ->join('persona','persona.persona_id','=','usuarios.persona_id')
            ->select('mensajes.mensaje_id','mensajes.chat_id','mensajes.usuario_id','mensajes.mensaje','persona.nombre','persona.apellidos')
            ->where('chats.chat_id','=',$chat_id)
            ->where('mensajes.estado','=',0)
            ->get();
        if (count($mensaje)!=0) {
            return response()->json($mensaje, 200);
        } else {
            $data = array(
                'mensaje' => 'mensajes no existen',
                'descripcion' => 'descripcion es null'
            );
            return response()->json($data, 200);
        }
    }

}
