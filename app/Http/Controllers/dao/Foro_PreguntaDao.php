<?php

namespace App\Http\Controllers\dao;



use Illuminate\Support\Facades\DB;
use App\Models\Foro_Pregunta;

class Foro_PreguntaDao
{
    function altaPregunta($foro)
    {
        DB::beginTransaction();
                try {
                    $foro->save();
                    $data=array(
                        'mensaje'=>'Pregunta creada con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
                return response()->json($data);
    }

    function listarPreguntas()
    {
        $foro = Foro_Pregunta::all()->where('estado','<>',true);
        return response()->json($foro);
    }

    function listaPregunta($id)
    {
        $foro = Foro_Pregunta::all()->where('foro_pregunta_id','=',$id)->where('estado','=',false);
        if(json_decode($foro, true)){
            return response()->json($foro);
        }
        else {
            $data= array(
                'mensaje'=>'No existe la pregunta que busca'
            );
            return response()->json($data);
        }
    }

    function actualizarPregunta($id,$pregunta,$usuario_id)
    {
        $foro=Foro_Pregunta::find($id);
        if($foro->usuario_id == $usuario_id){
            $foro->usuario_id=$usuario_id;
            $foro->pregunta=$pregunta;
            $foro->estado=false;
            DB::beginTransaction();
            try {
                $foro->save();
                $data=array(
                    'mensaje'=>'Pregunta actualizada con exito',
                    'descripcion'=>'exito'
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'mensaje'=>'Error al realizar la transaccion',
                    'descripcion'=>'fallo'
                );
                DB::rollback();
            }
        }
        else{
            $data=array(
                'mensaje'=>'El usuario no tiene acceso a la pregunta'
            );

        }
        return response()->json($data,200);
    }

    function eliminarPregunta($id,$usuario_id)
    {
        $foro=Foro_Pregunta::find($id);
        if($foro->usuario_id == $usuario_id){
            $foro->usuario_id=$usuario_id;
            $foro->estado=true;
            DB::beginTransaction();
            try {
                $foro->save();
                $data=array(
                    'mensaje'=>'Pregunta eliminada con exito',
                                'descripcion'=>'exito'
                    );
                DB::commit();
                } 
            catch (Exception $e) {
                $data=array(
                    'mensaje'=>'Error al realizar la transaccion',
                                'descripcion'=>'fallo'
                        );
                DB::rollback();
                }
            }
            else{
                $data=array(
                    'mensaje'=>'El usuario no tiene acceso a la pregunta'
                );
            }
            return response()->json($data);
    }
}
