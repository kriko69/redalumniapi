<?php

namespace App\Http\Controllers\dao;



use App\Models\Usuario;
use Illuminate\Support\Facades\DB;

class AmigoDao
{
    function obtenerUsuario($usuario_id)
    {
        return Usuario::find($usuario_id);
    }
    function listarMisAmigosPorId($id_amigos)
    {
        return DB::table('usuarios')
            ->join('persona','persona.persona_id','=','usuarios.persona_id')
            ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera','usuarios.usuario_id')
            ->whereIn('usuarios.usuario_id',$id_amigos)
            ->get();
    }
}
