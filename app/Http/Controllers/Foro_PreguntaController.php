<?php

namespace App\Http\Controllers;

use App\Models\Foro_Pregunta;
use \Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\Foro_PreguntaBl;

class Foro_PreguntaController extends Controller
{
    public function postForo_Pregunta(Request $request){  
        $validator = [
            'pregunta' => 'required'
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorForo = Validator::make($request->all(),$validator,$customMessages);
        if ($validatorForo->fails()) {
            $data=array(
                'errores'=>$validatorForo->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if($payload){
            $pregunta=$request->json("pregunta");
            $id_usuario = $payload->sub;
            //$id = Usuario::find($id_usuario); te devuelve todo como select
            if(!is_null($pregunta)){
                $foro = new Foro_Pregunta();
                $foro->usuario_id=$id_usuario;
                $foro->pregunta=$pregunta;
                $foro->estado=false;
            
                $bl = new Foro_PreguntaBl();
                $data = $bl->agregarPregunta($foro);
                return response()->json($data,200);
                
            }
            else{
                $data=array(
                    'mensaje'=>'Pregunta no creada',
                    'descripcion'=>'No hizo una pregunta'
                );
            }
        }
        else{
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }      
    
    }
    public function getForo_Pregunta(Request $request){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new Foro_PreguntaBl();
            $data = $bl->listarPreguntas();
            return response()->json($data,200);
        }
    }
    
    public function getForo_Pregunta_especifica(Request $request,$id){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new Foro_PreguntaBl();
            $data = $bl->listaPregunta($id);
            return response()->json($data);
        }
    }
    
    public function actualizarPregunta(Request $request,$id){
        $validator = [
            'pregunta' => 'required'
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorForo = Validator::make($request->all(),$validator,$customMessages);
        if ($validatorForo->fails()) {
            $data=array(
                'errores'=>$validatorForo->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $pregunta=$request->json("pregunta");
                $usuario_id = $payload->sub;
                $bl = new Foro_PreguntaBl();
                $data = $bl->actualizarPregunta($id,$pregunta,$usuario_id);
                return response()->json($data);
                
            }
    }
    
    public function eliminarPregunta(Request $request,$id){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $usuario_id = $payload->sub;
                if(!is_null($usuario_id)){
                    $bl = new Foro_PreguntaBl(); 
                    return response()->json($bl->eliminarPregunta($id,$usuario_id));               
                }
                else{
                    $data=array(
                        'mensaje'=>'Pregunta no se pudo eliminar',
                        'descripcion'=>'No encontro una pregunta'
                    );
                }
            }
    }
}
