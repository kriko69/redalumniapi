<?php

namespace App\Http\Controllers;

use App\Models\Noticias;
use \Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class noticiasController extends Controller
{
    public function getNoticias(){

        $noticias=new Noticias();
        $dato=Noticias::all();
        return response()->json($dato,200);

    }

    /**
     * @param Request $request
     */
    public function publicacion(Request $request)
    {
        $rules=[
            "titulo" => "required",
            "descripcion" => "required",
            "ubicacion" => "required"
        ];
        $mensaje=[
            'required'=>'El atributo :attribute es requerido'
        ];

        $validator=Validator::make($request->all(),$rules,$mensaje);
        if($validator->fails())
        {
            return $validator->errors();
        }
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $titulo=$request->json("titulo");
            $descripcion=$request->json("descripcion");
            $ubicacion=$request->json("ubicacion");
            $noticia = new Noticias();
            $noticia->usuario_id = $payload->sub;
            $noticia->titulo = $titulo;
            $noticia->descripcion = $descripcion;
            $noticia->ubicacion = $ubicacion;
            $noticia->estado = false;

            DB::beginTransaction();
            try {
                $noticia->save();
//            return response()->json("hola");
                $data=array(
                    'mensaje'=>'Noticia creada con exito',
                    'descripcion'=>'exito'
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'mensaje'=>'Error al realizar la transaccion',
                    'descripcion'=>'fallo'
                );
                DB::rollback();
            }
            return response()->json($data);
        }

    }



}
